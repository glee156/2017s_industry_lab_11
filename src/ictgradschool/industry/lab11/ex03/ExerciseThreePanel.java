package ictgradschool.industry.lab11.ex03;

import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 379, 386, 7);
        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // TODO Draw a house, as shown in the lab handout.

        //draw triangle for roof
        g.setColor(ROOF_COLOR);
        int[] xpoints = {left, left + 5*size, left + 10*size};
        int[] ypoints = {top + 5*size, top, top + 5*size};
        g.drawPolygon(xpoints, ypoints, 3);
        g.fillPolygon(xpoints, ypoints, 3);

        g.setColor(Color.black);
        g.drawPolygon(xpoints, ypoints, 3);

        //draw rectangle for body
        g.setColor(MAIN_COLOR);
        g.drawRect(left, top + 5*size, 10*size, 7*size);
        g.fillRect(left, top + 5*size, 10*size, 7*size);

        g.setColor(Color.black);
        g.drawRect(left, top + 5*size, 10*size, 7*size);

        //draw rectangles for windows
        g.setColor(WINDOW_COLOR);
        g.fillRect(left+(size*7), top + size*7, size*2, size*2);
        g.fillRect(left+size, top + size*7, size*2, size*2);

        g.setColor(Color.black);
        g.drawRect(left+(size*7), top + size*7, size*2, size*2);
        g.drawRect(left+size, top + size*7, size*2, size*2);

        //draw lines for window on left
        g.drawLine(left+size, top + size * 8, left + size * 3, top + size * 8);
        g.drawLine(left + size * 2, top + size * 7, left + size * 2, top + size * 9);

        //draw lines for window on right
        g.drawLine(left + size * 7, top + size * 8, left+ size * 9, top + size * 8);
        g.drawLine(left + size * 8, top + size * 7, left + size * 8, top + size * 9);

        //draw and fill rectangles for door
        g.drawRect(left + size * 4, top + size * 8, size * 2, size * 4);
        g.setColor(DOOR_COLOR);
        g.fillRect(left + size * 4, top + size * 8, size * 2, size * 4);

        //draw and fill polygon for chimney
        int[] chimneyX = {left+size*7, left+size*8, left+size*8,left+size*7};
        int[] chimneyY = {top + size, top + size, top + size*3, top + size*2};
        g.setColor(Color.black);
        g.drawPolygon(chimneyX, chimneyY, 4);
        g.setColor(CHIMNEY_COLOR);
        g.fillPolygon(chimneyX, chimneyY, 4);
    }
}