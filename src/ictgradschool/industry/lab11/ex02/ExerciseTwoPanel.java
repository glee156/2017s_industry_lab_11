package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{
    JButton addButton = new JButton("Add");
    JButton subtractButton = new JButton("Subtract");
    JTextField inputOne = new JTextField(10);
    JTextField inputTwo = new JTextField(10);
    JTextField outputResult = new JTextField(20);
    JLabel resultLabel = new JLabel("Result: ");

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        this.add(inputOne);
        this.add(inputTwo);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultLabel);
        this.add(outputResult);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == addButton){
            String dataOne = inputOne.getText();
            String dataTwo = inputTwo.getText();

            double valueOne = Double.parseDouble(dataOne);
            double valueTwo = Double.parseDouble(dataTwo);

            double result = valueOne + valueTwo;
            double rounded = roundTo2DecimalPlaces(result);

            outputResult.setText("" + rounded);
        }
        else if(e.getSource() == subtractButton){
            String dataOne = inputOne.getText();
            String dataTwo = inputTwo.getText();

            double valueOne = Double.parseDouble(dataOne);
            double valueTwo = Double.parseDouble(dataTwo);

            double result = valueOne - valueTwo;
            double rounded = roundTo2DecimalPlaces(result);

            outputResult.setText("" + rounded);
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}