package ictgradschool.industry.lab11.ex04;

import javax.jnlp.DownloadService;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import static java.awt.event.KeyEvent.*;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private  Balloon balloon;
    private ArrayList<Balloon> balloonList;
    private  JButton moveButton;
    private Direction direction;
    private Timer timer;
    private ArrayList<Timer> timerList;


    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        //drawing one balloon
        //this.balloon = new Balloon(30, 50);

        balloonList = new ArrayList<>();
        Balloon balloonOne = new Balloon(30, 60);
        Balloon balloonTwo = new Balloon(30,100);
        this.balloonList.add(balloonOne);
        this.balloonList.add(balloonTwo);

//        this.moveButton = new JButton("Move balloon");
//        this.moveButton.addActionListener(this);
//        this.add(moveButton);

        addKeyListener(this);

        //timer for one balloon
        this.timer = new Timer(150, this);

        //timer for many balloons not working... :(
        timerList = new ArrayList<>();
        Timer timerOne = new Timer(150, this);
        Timer timerTwo = new Timer(150, this);
        this.timerList.add(timerOne);
        this.timerList.add(timerTwo);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
//        requestFocusInWindow();
//        balloon.move();
//        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //for drawing one balloon
        //balloon.draw(g);

        //for drawing a list of balloons
        for(Balloon b : balloonList){
            b.draw(g);
        }
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        //timer for one balloon
        timer.start();

        //timer for multiple balloons not working...
         /*for (int i = 0; i < balloonList.size(); i++) {
            Timer currentTimer = timerList.get(i);
            currentTimer.start();

        }*/

        switch(e.getKeyCode()){
            case VK_UP: direction = Direction.Up;
            break;
            case VK_DOWN: direction = Direction.Down;
            break;
            case VK_LEFT: direction = Direction.Left;
            break;
            case VK_RIGHT: direction = Direction.Right;
            break;
            case VK_S: timer.stop();
            break;
            //add new balloon with d button!
            case VK_D: balloonList.add(new Balloon(40, 60));
            break;
        }
        for(Balloon b: balloonList) {
            b.setDirection(direction);
            b.move();
            repaint();
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}