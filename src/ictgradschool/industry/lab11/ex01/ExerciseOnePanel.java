package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    JButton bmiButton = new JButton("Calculate BMI");
    JButton maxButton = new JButton("Calculate max weight");
    JTextField bmiText = new JTextField(10);
    JTextField maxText = new JTextField(10);
    JTextField heightText = new JTextField(10);
    JTextField weightText = new JTextField(10);

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel height = new JLabel("Height in metres: ");
        JLabel weight = new JLabel("Weight in kilograms:  ");
        JLabel bmi = new JLabel("Your body mass index is: ");
        JLabel max = new JLabel("Your maximum weight for your height is: ");
        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(height);
        this.add(heightText);
        this.add(weight);
        this.add(weightText);
        this.add(Box.createVerticalStrut(10));
        this.add(bmiButton, BorderLayout.LINE_START);
        this.add(bmi);
        this.add(bmiText);
        this.add(Box.createVerticalStrut(10));
        this.add(maxButton, BorderLayout.LINE_START);
        this.add(max);
        this.add(maxText);
        this.add(Box.createVerticalStrut(10));

        // TODO Add Action Listeners for the JButtons
        bmiButton.addActionListener(this);
        maxButton.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        if(event.getSource() == bmiButton){
            String heightData = heightText.getText();
            double heightValue = Double.parseDouble(heightData);
            String weightData = weightText.getText();
            double bmiData = Double.parseDouble(weightData) / (heightValue * heightValue);
            double rounded = roundTo2DecimalPlaces(bmiData);
            bmiText.setText("" + rounded);
        }
        else if(event.getSource() == maxButton){
            String heightData = heightText.getText();
            double heightValue = Double.parseDouble(heightData);
            double maxData = 24.9 * heightValue * heightValue;
            double rounded = roundTo2DecimalPlaces(maxData);
            maxText.setText("" + rounded);
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}